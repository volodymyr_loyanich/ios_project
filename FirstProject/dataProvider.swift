//
//  dataProvider.swift
//  FirstProject
//
//  Created by Vladimir on 17.07.17.
//  Copyright © 2017 Vladimir. All rights reserved.
//

import UIKit
import Alamofire
import Foundation

class dataProvider {
//    func getCategory() ->  [String] {
    
    static func getAdvertTypes(_ f:@escaping (_ categoryDictionary : [String:String])->()){
        Alamofire.request("https://dom.ria.com/ru/default/index/simplesearchform?json=1&with_lang=1",
                      method: .get,
                      parameters: nil,
                      encoding: JSONEncoding.default)
            .responseJSON { response in
                //print(response)
                //to get status code
                if let status = response.response?.statusCode {
                    switch(status){
                    case 201:
                        print("example success")
                    default:
                        print("error with response status: \(status)")
                    }
                }
                
                //to get JSON return value
                if let JSONFromServer = response.result.value {
                    if let JSON = JSONFromServer as? [String: Any]{
                        if let advert_types = JSON["advert_types"] as? [String: String]{
//                                return advert_types
                            f(advert_types)
                        }
                    }
                }
        }
    }
}
