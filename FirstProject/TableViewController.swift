//
//  ViewController.swift
//  FirstProject
//
//  Created by Vladimir on 11.07.17.
//  Copyright © 2017 Vladimir. All rights reserved.
//

import UIKit
import Foundation

//let list:[String] = []


class TableViewController: UITableViewController {
    var advertTypesValue : [String] = []
    var advertTypesDictionary = [String: String]()
    var myIndex = 0
    
    @IBOutlet var advertTypeTableView: UITableView!

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) ->Int {
        var count:Int?
        count =  advertTypesValue.count
        return count!
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) ->UITableViewCell {
        let cell = UITableViewCell(style : UITableViewCellStyle.default, reuseIdentifier : "cell")
        cell.textLabel?.text = advertTypesValue[indexPath.row]
        return(cell)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        dataProvider.getAdvertTypes() {dic in
            self.advertTypesDictionary = dic
                for (key ,value) in dic {
                    print(key)
                    self.advertTypesValue.append(value)
                }
            print(self.advertTypesValue)

            self.tableView.reloadData()
            //            self.categoryBtn.setTitle("Button Title", for: .normal)
            
            //            self.categoryBtn.setTitle("Button Title",for: .normal)
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        myIndex = indexPath.row
        
        performSegue(withIdentifier: "setAdvertType", sender: self)
    }           
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepare(for segue: UIStoryboardSegue,
                 sender: Any?){
        if segue.identifier == "setAdvertType" {
            let controller = (segue.destination as! MainViewController)
            controller.advert_types_name = advertTypesValue[myIndex]
//            controller.category_id = categoryDictionary
        }
    }
    
}

